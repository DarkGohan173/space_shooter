{
    "id": "41703d5b-4909-4d58-87c1-aa02cf8d8dbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43345021-d8f5-47a7-99a4-7b78b1b76a10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41703d5b-4909-4d58-87c1-aa02cf8d8dbb",
            "compositeImage": {
                "id": "e542522f-21f6-42fb-befb-78b183e3d17c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43345021-d8f5-47a7-99a4-7b78b1b76a10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec851ec9-24c6-4666-a5d6-735bc14478ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43345021-d8f5-47a7-99a4-7b78b1b76a10",
                    "LayerId": "8c0c4b6c-3996-4c0c-8d89-114b08fe8e56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8c0c4b6c-3996-4c0c-8d89-114b08fe8e56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41703d5b-4909-4d58-87c1-aa02cf8d8dbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}